Smart Recruiting
===

## Table of contents
[TOC]

## Context

Smart Recruiting is a project powered by Disrupt Campus, an online platform mad to help companies to be in touch with students and educational institution. Our project sponsor is Anthony Geourjon, the EDCampus developper.

## Project purpose

Our goal was to pursue the project of INFO5 students, an online site that uses deep learning algorithm in order to guide companies trying to post traineeship offers. Since there is a lot of sectors in the UGA, recruiters do not know where to send their offers to reach the largest number of people among those who could eventually match the required qualifications.

## Course of action

After the first meeting with Anthony, our contact with Disrupt Campus, it was decided that the last project was poorly documented, hence difficult to reuse. Furthermore, it lacked some crucial points as the accuracy of the result, and also it was showing only one result where he would like to see at least the 3 best matches. Lastly, the code was not really functionnal as the libraries used improved and he told us to update and fix it.

## Choices made

### Database Model 

1. Updating the database model:

As the project was already in an advanced state, a database model was already in place.

<u>Database initial diagram</u>:  

![](https://i.imgur.com/yrxQ6fQ.png)

After taking charge of the project, and with the objective of improving it, we decided to update this model. This also involved an update of the API that is directly linked to the model.

<u>Database final diagram</u>:  

![](https://i.imgur.com/m4Im0qy.png)

As we can see, the main change is the removal of the "Equipe" table.

Indeed, there was no concrete evidence of using this table in the back end. It was only used as an association class to link a field with a prediction.

In the new model, the association is made directly using the prediction class. This class allows you to link an offer to a field by adding a score between 0 and 100.

In addition, a prediction contains a boolean to determine if a field is used as a reference to a given offer. This prediction thus has a score of 100.

2. Updating the managers: 

When we started the project, we noticed that all database calls were made in a single class (DatabaseManager).

To improve the readability and maintainability of the project, we created 1 manager per object in the database. 
Each of these classes only has the functions that allows it to make queries on the table it represents.

In addition, when creating these managers, we updated the methods that were no longer functional with the new model.

This step also enabled us to remove many methods that were duplicated or simply not used.

Once the managers were reorganized, we were able to update the corresponding API.

3. Updating the API:

The API (Application Programming Interface) allows to link the 2 front-end and back-end projects. It is displayed in the file route.py, this file contains all the paths that are called by the front-end to access, update, etc. the information in the database.

Since we noticed earlier that there were many unused functions, we wanted to check that all roads were used on the front-end side.

So we searched for the paths used by the front end and were able to reduce the size of the API by a third by removing unused paths.

Regarding the remaining paths, modifications were made because, following the updating of certain functions of managers, they no longer had the exact same return value.

For instance:  
Under the former model, an offer had a prediction for only one training. With the new database and machine learning model, when evaluating an offer, it obtains a prediction for each of the known trainings of the system.

### Preprocess

In this project, the pre-process step is used to build a model, called *Word2Vec*, that allows to build a word vocabulary and to associate a vector representation (a vector of floating numbers) to each of these words.

The *Word2Vec* model learns from a list of sentences (a sentence being a list of words) the vector representations of words in an $n$-dimensional space, where $n$ is therefore the size of the vectors. This model ensures that words with a common meaning or context are close in the $n$-dimensional space. 

Here is an example that generates vector representations for the words *network*, *system* and *goat*:

![](https://i.imgur.com/yATPJxl.png)

This model therefore does not need to know the training related to the conventions.

#### Fonctionnement

At first, the preprocess model *Word2Vec* was trained only on agreements. The model construction process took these agreements, tokenized the words of these agreements and trained the preprocessing model from these tokenized words (figure below).

![](https://i.imgur.com/KsCASOl.png)



We changed this process to improve the performance of the pre-processing model:

![](https://i.imgur.com/0x6hBjn.png)

Steps modified/added:
1. **Inputs :** The *Wikipedia* corpus has been added as an input so that the model can learn the vector representation of a very large number of words and can more effectively deduce the similarities between them.
2. **Tokenization :** Originally, tokenization was done by removing all the dots and splitting the character strings by whitespaces to find the words. We added the management of a large part of possible punctuation using regular expressions and used a plugin allowing to tokenize words efficiently according to more specific rules than mere whitespaces to take into account all the French language subtleties.
3. **Stemming :** We used a [stemming](https://en.wikipedia.org/wiki/Stemming) algorithm to keep only the word root (to keep only important word information) and give the same representation to the words *informatic* and *informatics* for instance.



#### Results

Thanks to a dimension reduction algorithm we can visualize the vector representations of words in a 2D space (two vectors close in the $n$-dimensional space will also be close in the 2D space).

Below is a vector representation of the first 500 words of the model *Word2Vec* trained on conventions and on the *Wikipedia* corpus (*2 million lines were taken from more than 30 million total lines*): 

![](https://i.imgur.com/FU35sZz.png)
> A zoom is made on a small area of the space to visualize the words having a close meaning. Here we can see that the model has learned the lexical field of computing (words being stemmed): multimedia, audiovisual, telecommunication, linux, software, technology, network, system, platform, etc.

As a comparison, here is a representation with the *Word2Vec* template initially provided:

![](https://i.imgur.com/giuPKy4.png)
> This time the image is zoomed to a quarter of the total representation (because there is much less vocabulary), the model has learned very little about the similarities between words, the lexical fields are mixed.

Lastly, we can note that by using the *Wikipedia* corpus to train the *Word2Vec* preprocessing model, we significantly increase the number of words in our vocabulary:

|          | Vocabulary size | 
| -------- | -------- | 
| Old model| 317      | 
| New model| 6007     | 

***This makes it possible to match a vector representation to words never seen before in the corpus of conventions provided, as well as to know their meaning thanks to the Wikipedia corpus.***



### Convolutional Neural Networks

We identified several problems during the project and code handling process:
- The accuracy measurement of the learning template was biased. The evaluation of the model was done with exactly the same offers as during the training phase;
- The program execution was very slow. After 1.5 hours the program stopped because of an excessive amount of RAM used (*PC : 8GB RAM, i5-7300HQ CPU @ 2.50GHz*);
- The code is poorly organized and documented. As a matter of fact, all the part dedicated to the neural network comes from a [Github code](https://github.com/dennybritz/cnn-text-classification-tf/blob/master/eval.py) that is growing old compared to all Tensorflow's features that have been implemented over the last years;
- When predicting an agreement, only one formation, the best one, was returned.

The slowness and RAM problems of this program were due to the loading of the *Word2Vec* model on the disk each time an offer was processed and the vector representation deserialization step of the offers stored on the database. 

Indeed, they generated all offers vector representations during preprocessing, serialized them to store them on the database and then retrieve them on the database to deserialize them. As these vector representations are only used in the model input process, we compute them directly in this program without storing them on the database. This has allowed us to reduce the offers processing time by a factor of 7. On more than 6000 offers, the impact is huge!

Then, we were able to compute the true accuracy of the model by assessing it on previously **unprocessed** offers, **the accuracy of their model is 46%**.

The code used for training, evaluation and agreement prediction being difficult to learn and not very scalable, we preferred to rewrite this part using the Keras framework, which also allows us to focus on the architecture of our model and the adjustment of its parameters.

We provided the features of the new model through a *Model* class:

![](https://i.imgur.com/eWLJddi.png)

This class defines the convolutional model architecture used, allowing to: 
- train and evaluate it with agreements related to training;
- load the model stored in a file;
- predict the trainings related to an agreement by providing a matching percentage between the agreement and all the trainings managed by the model.  

With this new model, **we were able to obtain an accuracy of 95%**.


### Documentation, unit testing
#### Documentation

We generated a [project documentation](https://projets-info4.gricad-pages.univ-grenoble-alpes.fr/18-19/21/backend/) using *Sphynx*. This documentation describes the installation, deployment and contribution steps as well as detailed documentation of the different project packages (package documentation, functions and classes).

#### Unit testing

In order to achieve continuous integration, we implemented unit tests.
These were performed using the unittest library of python.
The database model unit tests were partially performed. On the other hand, the ones regarding the `deeplearning` package are all functional.

#### SonarQube

SonarQube was deployed on this project, we were able to monitor the project's progress and improve some areas. For instance: 
- We went from 2700 lines to 1500 lines which improved code maintainability and readability;
- We solved part of the technical debt (3h -> 30min);
- We also added documentation (from 20% to 44% of documentation).

## Overview
### What has been done

- Deep learning model update
- Database model update
- API update
- Association of a forecast for each training and the pertinence rate display.

### What has yet to be done

- Detect errors and update the front-end;
- Complete the database model unit tests.

![](https://i.imgur.com/cTyBNgV.png)
