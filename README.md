# SmartRecruiting

Notre liste de tâches à faire est disponible sous la forme [d'*issues* Gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/groups/Projets-INFO4/18-19/21/-/boards) avec différents labels :
- **Urgency** : doit être fait avant tout autres tâches
- **Test** : unit testing
- **TODO** : à faire
- **Documentation**

Une document dans le backend (*./docs/*)` est aussi disponible avec des descriptions de chaques modules Python avec notament les *updates* effectuées au fur et à mesure et expliquées plus précisement  que dans ce fichier succinct. 

## Semaine 6


[Merge request #3 acceptée](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/merge_requests/3)

> - Unit tests for preprocess.py  
> - preprocess.py code documentation  
> - template documentation for database_handler.py
> - Add our config parameters to the app.config (Flask app config)  
> - Add Sphinx documentation :
> 
>    - Create docs/ directory for the documentation and init a the Sphinx documentation (see conf.py)  
>    - Create the documentation structure (install, contribution, code documentation)  
>    - Move some parts of the actual documentation to this new documentation  
>    - Publish with readthedocs : https://smartrecruiting-back.readthedocs.io/en/latest/index.html (so for the moment, it's link to the preprocess branch, we have to change that when we'll merge)  

Tâches en cours (issues placées en *doing* en début de semaine):
- [train - Migrate to keras](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/issues/24)
- [Fix packages version in requirement.txt](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/issues/20)
- [Provide clean installation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/issues/7)
- [Drop tables --init (and --renit ??)](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/issues/10)
- [Resolve PEP8 issues in docs/conf.py](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/issues/22)

## Semaine 5
Réunion avec notre client pour faire le point et discuter des prochaines choses à faire. Mise en place d'issues sur Gitlab. 

[Merge request #1 acceptée](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/merge_requests/1) :

> Ajout d'un fichier de configuration (config.yml) pour éviter les valeurs en brutes et faire les modifications plus rapidement sans modifier à 20 endroits différents  
> Séparation du fichier "pretraitement" en deux fichiers :
> 
> pretraitement : preprocess le texte  
> database_handler : opérations en lien avec la BDD (ajout des offres, contacts, etc ; update ; etc)
> 
> Modifications du code des fichiers "preprocess/" pour s'accorder au "Style Guide" de python : pep8
> 
> Modification de quelques bouts de code python (boucle for)  
> Ajout de quelques commentaires plus explicites  
> Test file (plot words vectors)  

[Merge request #2 acceptée](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/18-19/21/backend/merge_requests/2) :
> Preprocess des mots plus avancé : pouctuation, racinisation des mots, utilisation de la fonction de tokenization de NLTK (un module pour la natural language processing), etc  
> Train word2vec model sur un corpus de texte wikipedia bien plus grand qu'actuellement  
> Ajout d'un paramètre pour gérer l'init du word2vec indépendamment de l'init du CNN pour les prédictions de formations  
> Ajout d'un fichier de contribution pour définir les conventions utilisées  
> Ajout d'une fonction pour visualiser les descriptors des mots en 2D  
> Refactor tout le code en PEP8 (style guide)  

Link the project to [SonarCloud](https://sonarcloud.io/dashboard?id=smartrecruiting_back)

## Semaine 4
- Tutoriels plus poussés sur le traitement automatique du langage naturel (lemmatisation, racinisation, tolenization, etc).
- Tests effectués pour améliorer les prétraitements actuellement effectué.

## Semaine 3
- Analyse de performances et d'optimisation pour detecter les problèmes les lenteurs et de gestion de RAM.
Conclusion : le cast des déscripteurs de fichiers en string pour les stocker dans le BDD prends ~80% du temps de preprocess. La récupération des offres consomme une grande quantité de RAM (à vérifier plus précisement)
- Tutoriels : Tensorflow, les bases du deep learning et le preprocess des mots.

## Semaine 2
- Résolution des problèmes d'installation liés à la documentation obsolète du code. 
- Mise en place de la base de données sur nos machines.
- Prise en main du code existant. Débuggage pour voir le cheminement des différentes features.
- Pistes de solutions pour développer les exigences de notre client

## Semaine 1
- Rencontre avec le client (Disrupt Campus, Anthony Geourjon). Présentation des exigences : 
    - Pouvoir communiquer avec le service via une API
    - Plusieurs formations par réponse avec pourcentage de matching avec l'offre
    - Réponses plus pertinentes qu'actuellement
- Mise en place d'un Slack pour communiquer avec le client.  
- Récuparation des fichiers csv nécessaire pour faire fonctionner le code.
- Debrief ensemble pour la suite du développement


## Semaine 0
- Téléchargement du code
- Installation des outils. 
- Lecture des documents.
- Contact avec le client
